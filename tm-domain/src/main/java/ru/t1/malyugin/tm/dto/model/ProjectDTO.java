package ru.t1.malyugin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project", schema = "tm")
public final class ProjectDTO extends AbstractWBSModelDTO {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String description = "";

    public ProjectDTO(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        boolean isDescription = StringUtils.isBlank(description);

        result += String.format("%s: %s IN STATUS '%s'", id, name, status.getDisplayName());
        result += (isDescription ? " -> " + description : "");
        return result;
    }

}