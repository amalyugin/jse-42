package ru.t1.malyugin.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;


@Getter
@Setter
@NoArgsConstructor
public class ProjectUpdateByIdResponse extends AbstractResponse {

    @Nullable
    private ProjectDTO project;

    public ProjectUpdateByIdResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

}