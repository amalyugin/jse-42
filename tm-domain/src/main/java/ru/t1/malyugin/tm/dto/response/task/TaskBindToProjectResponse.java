package ru.t1.malyugin.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;


@Getter
@Setter
@NoArgsConstructor
public class TaskBindToProjectResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

    public TaskBindToProjectResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

}