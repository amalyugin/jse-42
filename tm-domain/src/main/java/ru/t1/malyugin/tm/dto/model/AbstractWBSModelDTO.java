package ru.t1.malyugin.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.model.IWBS;
import ru.t1.malyugin.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractWBSModelDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @Column
    @NotNull
    protected Date created = new Date();

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    protected String name = "";

}