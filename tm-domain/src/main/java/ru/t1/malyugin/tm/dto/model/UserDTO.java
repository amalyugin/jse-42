package ru.t1.malyugin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user", schema = "tm")
public class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    private String login;

    @NotNull
    @Column(name = "password")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column(name = "locked_flg")
    @NotNull
    private Boolean locked = false;

    @Override
    @NotNull
    public String toString() {
        return String.format("USER %s: %s", id, login);
    }

}