package ru.t1.malyugin.tm.command.user;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.malyugin.tm.api.endpoint.IUserEndpoint;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.dto.model.UserDTO;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return getEndpointLocator().getUserEndpoint();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return getEndpointLocator().getAuthEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderProfile(@Nullable final UserDTO user) {
        if (user == null) return;
        @NotNull String result = "";
        @Nullable final String email = user.getEmail();
        @Nullable final String firstName = user.getFirstName();
        @Nullable final String lastName = user.getLastName();
        @Nullable final String middleName = user.getMiddleName();
        @NotNull final String id = user.getId();
        @NotNull final String login = user.getLogin();
        @NotNull final String role = user.getRole().getDisplayName();
        boolean isEmail = !StringUtils.isBlank(email);
        boolean isFIO = (!StringUtils.isBlank(firstName) || !StringUtils.isBlank(lastName) || !StringUtils.isBlank(middleName));
        boolean isFirstName = !StringUtils.isBlank(firstName);
        boolean isLastName = !StringUtils.isBlank(lastName);
        boolean isMiddleName = !StringUtils.isBlank(middleName);

        result += ("ID: " + id);
        result += ("\nLOGIN: " + login);
        result += ("\nROLE: " + role);
        result += (isEmail ? "\nEMAIL: " + email : "");
        result += (isFIO ? "\nFIO: "
                + (isFirstName ? firstName : "")
                + (isMiddleName ? " " + middleName : "")
                + (isLastName ? " " + lastName : "") : "");
        System.out.println(result);
    }

}