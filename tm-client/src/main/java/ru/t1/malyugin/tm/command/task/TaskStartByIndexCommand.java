package ru.t1.malyugin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.task.TaskStartByIndexRequest;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-start-by-index";

    @NotNull
    private static final String DESCRIPTION = "Start task by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");

        System.out.print("ENTER TASK INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextInteger();

        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken(), index);
        getTaskEndpoint().startTaskByIndex(request);
    }

}