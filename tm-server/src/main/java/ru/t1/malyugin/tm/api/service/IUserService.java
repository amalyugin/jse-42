package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    void clear();

    void removeById(
            @Nullable String id
    );

    @Nullable
    UserDTO findOneById(
            @Nullable String id
    );

    @NotNull
    List<UserDTO> findAll();

    int getSize();

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email,
            @Nullable Role role
    );

    UserDTO lockUserByLogin(
            @Nullable String login
    );

    UserDTO unlockUserByLogin(
            @Nullable String login
    );

    @NotNull
    UserDTO removeByLogin(
            @Nullable String login
    );

    @NotNull
    UserDTO removeByEmail(
            @Nullable String email
    );

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    UserDTO updateProfile(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @Nullable
    UserDTO findOneByLogin(
            @Nullable String login
    );

    @Nullable
    UserDTO findOneByEmail(
            @Nullable String email
    );

    @NotNull
    Boolean isLoginExist(
            @Nullable String login
    );

    @NotNull
    Boolean isEmailExist(
            @Nullable String email
    );

}