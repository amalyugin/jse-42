package ru.t1.malyugin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert(
            "INSERT INTO tm.tm_user (row_id, locked_flg, login, first_name, middle_name, " +
                    "last_name, password, email, role) " +
                    "VALUES (#{id}, #{locked}, #{login}, #{firstName}, #{middleName}, #{lastName}, " +
                    "#{passwordHash}, #{email}, #{role})"
    )
    void add(@NotNull UserDTO user);

    @Delete("DELETE FROM tm.tm_user")
    void clear();

    @Nullable
    @Select("SELECT * FROM tm.tm_user")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "locked", column = "locked_flg"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "passwordHash", column = "password")
    })
    List<UserDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm.tm_user WHERE row_id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "locked", column = "locked_flg"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "passwordHash", column = "password")
    })
    UserDTO findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT COUNT(*) FROM tm.tm_user")
    Integer getSize();

    @Delete("DELETE FROM tm.tm_user WHERE row_id = #{id}")
    void remove(@NotNull UserDTO user);

    @Nullable
    @Select("SELECT * FROM tm.tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "locked", column = "locked_flg"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "passwordHash", column = "password")
    })
    UserDTO findOneByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM tm.tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "locked", column = "locked_flg"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "passwordHash", column = "password")
    })
    UserDTO findOneByEmail(@NotNull @Param("email") String email);

    @Update(
            "UPDATE tm.tm_user SET locked_flg = #{locked}, first_name = #{firstName}, middle_name = #{middleName} " +
                    ", last_name = #{lastName}, password = #{passwordHash}, role = #{role} WHERE row_id = #{id}"
    )
    void update(@NotNull UserDTO user);

}