package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.util.HashUtil;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_USERS; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("TST_" + i);
            user.setPasswordHash("TST");
            user.setEmail("tst@mail.ru" + i);
            USER_REPOSITORY.add(user);
            USER_LIST.add(user);

            @NotNull final ProjectDTO project = new ProjectDTO("NEW", "NEW");
            project.setUserId(user.getId());
            @NotNull final TaskDTO task = new TaskDTO("NEW", "NEW");
            task.setUserId(user.getId());
            PROJECT_REPOSITORY.add(project);
            TASK_REPOSITORY.add(task);
        }
    }

    @After
    public void clearData() {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        for (@Nullable final UserDTO user : USER_LIST) USER_REPOSITORY.remove(user);
        TASK_LIST.clear();
        PROJECT_LIST.clear();
        USER_LIST.clear();
    }

    @Test
    public void testCreateUser() {
        final int initialNumberOfUsers = USER_SERVICE.getSize();
        final int expectedNumberOfUsers = initialNumberOfUsers + 4;
        @NotNull final UserDTO user1 = USER_SERVICE.create("NEW1", "PASS1", "MAIL1", null);
        @NotNull final UserDTO user2 = USER_SERVICE.create("NEW2", "PASS2", "MAIL2", Role.ADMIN);
        @NotNull final UserDTO user3 = USER_SERVICE.create("NEW3", "PASS3", null, null);
        @NotNull final UserDTO user4 = USER_SERVICE.create("NEW4", "PASS4", null, null);
        USER_LIST.add(user1);
        USER_LIST.add(user2);
        USER_LIST.add(user3);
        USER_LIST.add(user4);

        Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
        Assert.assertEquals("NEW1", user1.getLogin());
        Assert.assertEquals("MAIL1", user1.getEmail());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PASS1"), user1.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user1.getRole());
        Assert.assertEquals("NEW2", user2.getLogin());
        Assert.assertEquals("MAIL2", user2.getEmail());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PASS2"), user2.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user2.getRole());
    }

    @Test
    public void testCreateUserNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, UNKNOWN_STRING, UNKNOWN_STRING, null));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, UNKNOWN_STRING, UNKNOWN_STRING, Role.USUAL));

        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, null, UNKNOWN_STRING, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, null, UNKNOWN_STRING, Role.USUAL));

        @NotNull final UserDTO user = USER_LIST.get(0);
        Assert.assertThrows(LoginExistException.class, () -> USER_SERVICE.create(user.getLogin(), UNKNOWN_STRING, UNKNOWN_STRING, null));
        Assert.assertThrows(LoginExistException.class, () -> USER_SERVICE.create(user.getLogin(), UNKNOWN_STRING, UNKNOWN_STRING, Role.USUAL));

        Assert.assertThrows(EmailExistException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, UNKNOWN_ID, user.getEmail(), null));
        Assert.assertThrows(EmailExistException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, UNKNOWN_ID, user.getEmail(), Role.USUAL));
    }

    @Test
    public void testLockUserByLogin() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertNotNull(USER_SERVICE.lockUserByLogin(user.getLogin()));
            @Nullable final UserDTO actualUser = USER_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(true, actualUser.getLocked());
        }
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.lockUserByLogin(UNKNOWN_STRING));
    }

    @Test
    public void testUnlockUserByLogin() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertNotNull(USER_SERVICE.lockUserByLogin(user.getLogin()));
            @Nullable UserDTO actualUser = USER_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(true, actualUser.getLocked());
            Assert.assertNotNull(USER_SERVICE.unlockUserByLogin(user.getLogin()));
            actualUser = USER_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(false, actualUser.getLocked());
        }
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.unlockUserByLogin(UNKNOWN_STRING));
    }

    @Test
    public void testRemove() {
        int expectedNumberOfUsers = USER_SERVICE.getSize();
        for (@NotNull final UserDTO user : USER_LIST) {
            expectedNumberOfUsers--;
            Assert.assertNotEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertNotEquals(0, PROJECT_SERVICE.getSize(user.getId()));
            USER_SERVICE.removeById(user.getId());
            Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
            Assert.assertEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertEquals(0, PROJECT_SERVICE.getSize(user.getId()));
        }
    }

    @Test
    public void testRemoveByLogin() {
        int expectedNumberOfUsers = USER_SERVICE.getSize();
        for (@NotNull final UserDTO user : USER_LIST) {
            expectedNumberOfUsers--;
            Assert.assertNotEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertNotEquals(0, PROJECT_SERVICE.getSize(user.getId()));
            Assert.assertNotNull(USER_SERVICE.removeByLogin(user.getLogin()));
            Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
            Assert.assertEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertEquals(0, PROJECT_SERVICE.getSize(user.getId()));
        }
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(null));
    }

    @Test
    public void testRemoveByEmail() {
        int expectedNumberOfUsers = USER_SERVICE.getSize();
        for (@NotNull final UserDTO user : USER_LIST) {
            expectedNumberOfUsers--;
            Assert.assertNotEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertNotEquals(0, PROJECT_SERVICE.getSize(user.getId()));
            Assert.assertNotNull(USER_SERVICE.removeByEmail(user.getEmail()));
            Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
            Assert.assertEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertEquals(0, PROJECT_SERVICE.getSize(user.getId()));
        }
    }

    @Test
    public void testRemoveByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.removeByEmail(null));
    }

    @Test
    public void testSetPassword() {
        @NotNull final UserDTO user = USER_LIST.get(0);
        @NotNull final String pass = "NEWP";
        @Nullable final String passHash = HashUtil.salt(PROPERTY_SERVICE, pass);
        Assert.assertNotNull(USER_SERVICE.setPassword(user.getId(), pass));
        @Nullable final UserDTO actualUser = USER_SERVICE.findOneById(user.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(passHash, actualUser.getPasswordHash());
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(null, UNKNOWN_STRING));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(UNKNOWN_STRING, null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.setPassword(UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testUpdateProfile() {
        for (@NotNull final UserDTO user : USER_LIST) {
            @NotNull final String test = "TEST";
            Assert.assertNotNull(USER_SERVICE.updateProfile(user.getId(), null, null, test));
            Assert.assertNotNull(USER_SERVICE.updateProfile(user.getId(), null, test, null));
            Assert.assertNotNull(USER_SERVICE.updateProfile(user.getId(), test, null, null));
            @Nullable final UserDTO actualUser = USER_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(test, actualUser.getFirstName());
            Assert.assertEquals(test, actualUser.getMiddleName());
            Assert.assertEquals(test, actualUser.getLastName());
        }
    }

    @Test
    public void testUpdateProfileNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateProfile(null, UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.updateProfile(UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testFindOneByLogin() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertEquals(user.getId(), USER_SERVICE.findOneByLogin(user.getLogin()).getId());
        }
        Assert.assertNull(USER_SERVICE.findOneByLogin(null));
    }

    @Test
    public void testFindOneByEmail() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertEquals(user.getId(), USER_SERVICE.findOneByEmail(user.getEmail()).getId());
        }
        Assert.assertNull(USER_SERVICE.findOneByEmail(null));
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertEquals(true, USER_SERVICE.isLoginExist(user.getLogin()));
        }
        Assert.assertEquals(false, USER_SERVICE.isLoginExist(null));
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final UserDTO user : USER_LIST) {
            Assert.assertEquals(true, USER_SERVICE.isEmailExist(user.getEmail()));
        }
        Assert.assertEquals(false, USER_SERVICE.isEmailExist(null));
    }

}